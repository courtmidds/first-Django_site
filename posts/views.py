from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from .models import User, Post
from django.shortcuts import render,  get_object_or_404
from django.urls import reverse


class IndexView(generic.ListView):
    template_name = 'posts/index.html'
    context_object_name = 'list_all_users'

    def get_queryset(self):
        """Return the top users based off skill level"""
        return User.objects.all()


class DetailView(generic.DetailView):
    model = User
    template_name = 'posts/detail.html'


def vote(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    try:
        selected_post = user.post_set.get(pk=request.POST['post_id'])
    except (KeyError, Post.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'posts/detail.html', {
            'user': user,
            'error_message': "You didn't select a post.",
        })
    else:
        selected_post.likes += 1
        selected_post.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('posts:scoreboard', args=(user.id,)))


class ScoreboardView(generic.DetailView):
    model = User
    template_name = 'posts/scoreboard.html'
