from django.db import models


class User(models.Model):
    bio = models.CharField(max_length = 255)
    age = models.IntegerField()
    skillLevel = models.CharField(max_length = 100)
    location = models.CharField(max_length = 1000)

    def __str__(self):
        return self.bio + ' : ' + self.skillLevel


class Post(models.Model):
    User =  models.ForeignKey(User, on_delete= models.CASCADE)
    subject = models.CharField(max_length = 100)
    currentLoc = models.CharField(max_length = 1000)
    likes = models.IntegerField(default=0)

    def __str__(self):
        return self.subject
