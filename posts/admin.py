from django.contrib import admin
from .models import User, Post

# username: admin password: bob9100tre


class PostInline(admin.TabularInline):
    model = Post
    extra = 2


class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,          {'fields': ['bio']}),
        ('Skill Level', {'fields': ['skillLevel'], 'classes': ['collapse']}),
        ('Age',          {'fields': ['age'], 'classes': ['collapse']}),
        ("Location",          {'fields': ['location'], 'classes': ['collapse']})

    ]
    inlines = [PostInline]
    list_display = ('bio', 'skillLevel', 'location')
    list_filter = ['skillLevel']
    search_fields = ['location']


admin.site.register(User, UserAdmin)
